import os
import csv
import pandas as pd
import io
import tensorflow as tf
import keras
import nltk
from sklearn.model_selection import ParameterGrid
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Flatten, LSTM, Conv1D, MaxPooling1D, \
    Dropout, Activation, Conv2D, GlobalMaxPool1D, GlobalMaxPooling1D, Input
from keras.layers.embeddings import Embedding
from keras.utils import to_categorical
import numpy as np
import random
import matplotlib.pyplot as plt


#######################################
#PREPROCESSING

#IMPORTANT: Change it to your directory before you run it

train = pd.read_csv('./all/train.csv')
# Take a subset of training data for quickly testing functions
#train = train[1:10000]
test = pd.read_csv('./all/test.csv')
test_labels = pd.read_csv('./all/test_labels.csv')

# Take all test labels that are not -1
test = test[test_labels.toxic != -1]
test_labels = test_labels.drop(columns = ["id"])
test_labels = test_labels[test_labels.toxic != -1]

#train = pd.read_csv("C:/Users/Spiros/Desktop/Σχολή/Pattern 
# Recognition/asdf/all/train.csv", engine='python')
#test = pd.read_csv("C:/Users/Spiros/Desktop/Σχολή/Pattern 
# Recognition/asdf/all/test.csv", engine='python')

####Second way for reading the files if the first has a problem
#with open('train.csv', 'rb') as f:
#    train = csv.reader(f)
#with open('test.csv', 'rb') as f:
#    test = csv.reader(f)

#Transofrming the files into lists
train_comments = train["comment_text"].fillna("No Comment").values
test_comments = test["comment_text"].fillna("No Comment").values
#Creating the labels
labels = ["toxic", "severe_toxic", "obscene", "threat", "insult", "identity_hate"]
train_labels = train[labels].values
#Breakpoint "1" for testing

#Tokenization

#labels
number_labels = 6
tokenizer_labels = Tokenizer( num_words = number_labels,filters='!"#$%&()*+,-./:;<=>?@[\]^`{|}~ ' )
tokenizer_labels.fit_on_texts( labels )
labels_token = tokenizer_labels.texts_to_sequences( labels )

#comments
voc_size = 10000
tokenizer_words = Tokenizer( num_words = voc_size, filters='!"#$%&()*+,-./:;<=>?@[\]^`{|}~ ' ) # remark: size of
# vocab, depends on complexity of sentences
tokenizer_words.fit_on_texts( list(train_comments) + list(test_comments) )
train_comments_final = tokenizer_words.texts_to_sequences( train_comments )
test_comments_final = tokenizer_words.texts_to_sequences( test_comments )
dictionary = tokenizer_words.index_word

#Find the sentence with the maximum number of words
temp = (max(train_comments_final, key=len))
temp1 = train_comments_final.index(temp)


# pad X sequence only
max_length_sentence = 100
train_comments_processed = pad_sequences(train_comments_final, max_length_sentence, padding='post')
test_comments_processed = pad_sequences(test_comments_final,max_length_sentence,
                                        padding='post')

#Breakpoint "2" for testing
print(train_comments_processed[1])

#Transform our data into numpy arrays
train_comments_processed = np.array(train_comments_processed)
labels_token = np.array(labels_token)
experiment1 = np.array(train_labels)

#Transform labels to categorical data
#labels_token = to_categorical(labels_token, number_labels)
#experiment1 = to_categorical(train_labels, number_labels)

experimenttest = np.array(test_labels)
#experimenttest = to_categorical(experimenttest, number_labels)

embedding_vector_length = 32
resultlist = []
bestaccuracy = 0
bestparam = []

batch_size = [32, 64]
dropout = [0.5, 0.8]
padding = [50, 100]
activation = ['sigmoid', 'softmax']

param_grid = { 'batch_size': batch_size,
               'dropout': dropout,
               'padding': padding,
               'activation': activation,
               }


def runmodels(batch_size, dropout, padding, activation,
              ):
    global bestaccuracy
    global bestparam

    model = Sequential()
    model.add(Embedding(voc_size, embedding_vector_length,
                       input_length=max_length_sentence))
    model.add(LSTM(150))
    model.add(Dropout(dropout))
    model.add(Dense(padding, activation='relu'))
    model.add(Dense(number_labels, activation= activation))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.fit(train_comments_processed, experiment1, epochs=3, batch_size=batch_size, validation_split=0.1, shuffle=True)

    results = model.evaluate(test_comments_processed, experimenttest)
    prediction = model.predict(test_comments_processed)

    resultlist.append(list(param.values()) + [results[1]])
    print("RESULTSLIST")
    print(resultlist)

    if results[1] > bestaccuracy:
        print("BEST UNTIL NOW")
        print(results[1])
        print(param)

        bestaccuracy = results[1]
        bestparam = param
        submission_df = pd.DataFrame(columns=['id'] + labels)
        submission_df['id'] = test['id'].values
        submission_df[labels] = prediction
        submission_df.to_csv("./lstm_result.csv", index=False)


for param in list(ParameterGrid(param_grid)):
    print("PARAMETERS")
    print(param)
    runmodels(param['batch_size'], param['dropout'], param['padding'],
              param['activation'])

for result in resultlist:
    print(result)
print(bestaccuracy)
print(bestparam)





