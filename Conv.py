import os
import csv
import pandas as pd
import io
import tensorflow as tf
import keras
import nltk
from sklearn.model_selection import ParameterGrid
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Flatten, LSTM, Conv1D, MaxPooling1D, \
    Dropout, Activation, Conv2D, GlobalMaxPool1D, GlobalMaxPooling1D, Input
from keras.layers.embeddings import Embedding
from keras.utils import to_categorical
import numpy as np
import random
import matplotlib.pyplot as plt


#######################################
#PREPROCESSING

#IMPORTANT: Change it to your directory before you run it

train = pd.read_csv('./all/train.csv')
# Take a subset of training data for quickly testing functions
#train = train[1:10000]
test = pd.read_csv('./all/test.csv')
test_labels = pd.read_csv('./all/test_labels.csv')

# Take all test labels that are not -1
test = test[test_labels.toxic != -1]
test_labels = test_labels.drop(columns = ["id"])
test_labels = test_labels[test_labels.toxic != -1]

#train = pd.read_csv("C:/Users/Spiros/Desktop/Σχολή/Pattern 
# Recognition/asdf/all/train.csv", engine='python')
#test = pd.read_csv("C:/Users/Spiros/Desktop/Σχολή/Pattern 
# Recognition/asdf/all/test.csv", engine='python')

####Second way for reading the files if the first has a problem
#with open('train.csv', 'rb') as f:
#    train = csv.reader(f)
#with open('test.csv', 'rb') as f:
#    test = csv.reader(f)

#Transofrming the files into lists
train_comments = train["comment_text"].fillna("No Comment").values
test_comments = test["comment_text"].fillna("No Comment").values
#Creating the labels
labels = ["toxic", "severe_toxic", "obscene", "threat", "insult", "identity_hate"]
train_labels = train[labels].values
#Breakpoint "1" for testing

#Tokenization

#labels
number_labels = 6
tokenizer_labels = Tokenizer( num_words = number_labels,filters='!"#$%&()*+,-./:;<=>?@[\]^`{|}~ ' )
tokenizer_labels.fit_on_texts( labels )
labels_token = tokenizer_labels.texts_to_sequences( labels )

#comments
voc_size = 10000
tokenizer_words = Tokenizer( num_words = voc_size, filters='!"#$%&()*+,-./:;<=>?@[\]^`{|}~ ' ) # remark: size of
# vocab, depends on complexity of sentences
tokenizer_words.fit_on_texts( list(train_comments) + list(test_comments) )
train_comments_final = tokenizer_words.texts_to_sequences( train_comments )
test_comments_final = tokenizer_words.texts_to_sequences( test_comments )
dictionary = tokenizer_words.index_word

#Find the sentence with the maximum number of words
temp = (max(train_comments_final, key=len))
temp1 = train_comments_final.index(temp)


# pad X sequence only
max_length_sentence = 200
train_comments_processed = pad_sequences(train_comments_final, max_length_sentence, padding='post')
test_comments_processed = pad_sequences(test_comments_final,max_length_sentence,
                                        padding='post')

#Breakpoint "2" for testing
print(train_comments_processed[1])

#Transform our data into numpy arrays
train_comments_processed = np.array(train_comments_processed)
labels_token = np.array(labels_token)
experiment1 = np.array(train_labels)

#Transform labels to categorical data
#labels_token = to_categorical(labels_token, number_labels)
#experiment1 = to_categorical(train_labels, number_labels)

experimenttest = np.array(test_labels)
#experimenttest = to_categorical(experimenttest, number_labels)

embedding_vector_length = 32
resultlist = []
bestaccuracy = 0
bestparam = []

# batch_size = [32, 64]
# dropout = [0.5, 0.8]
# padding = [50, 100]
# activation = ['sigmoid', 'softmax']
# filter_amount = [50, 100, 200]
# filter_size = [3, 5, 8]

batch_size = [32]
dropout = [0.8]
padding = [100]
activation = ['sigmoid']
filter_amount = [50]
filter_size = [3]

param_grid = { 'batch_size': batch_size,
               'dropout': dropout,
               'padding': padding,
               'activation': activation,
               'filter_amount': filter_amount,
               'filter_size': filter_size }


def runmodels(batch_size, dropout, padding, activation, filter_amount,
              filter_size):
    global bestaccuracy
    global bestparam
    ######################################
    # Network architecture of CNN
    model = Sequential()
    model.add(Embedding(voc_size, embedding_vector_length,
                        input_length=max_length_sentence))
    model.add(Conv1D(filter_amount, filter_size))
    model.add(MaxPooling1D(2))
    model.add(Dropout(dropout))
    model.add(Flatten())
    model.add(Dense(padding, activation='relu'))
    # Use sigmoid activation for binary, softmax for multinomial
    model.add(Dense(number_labels, activation=activation))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=[
        'accuracy'])
    model.fit(train_comments_processed, experiment1, epochs=3,
              batch_size=batch_size, validation_split=0.1, shuffle=True)
    # we can also put cross-validation

    results = model.evaluate(test_comments_processed, experimenttest)
    prediction = model.predict(test_comments_processed)

    resultlist.append(list(param.values()) + [results[1]])
    print("RESULTSLIST")
    print(resultlist)

    if results[1] > bestaccuracy:
        print("BEST UNTIL NOW")
        print(results[1])
        print(param)

        bestaccuracy = results[1]
        bestparam = param
        submission_df = pd.DataFrame(columns=['id'] + labels)
        submission_df['id'] = test['id'].values
        submission_df[labels] = prediction
        submission_df.to_csv("./conv_result.csv", index=False)


for param in list(ParameterGrid(param_grid)):
    print("PARAMETERS")
    print(param)
    runmodels(param['batch_size'], param['dropout'], param['padding'],
              param['activation'], param['filter_amount'], param[
                  'filter_size'])

for result in resultlist:
    print(result)
print(bestaccuracy)
print(bestparam)

# ######################################
# # Network architecture of CNN
# filter_sizes = [1,2,3,5]
#
# model = Sequential()
# model.add(Embedding(voc_size, embedding_vector_length,
#                     input_length=max_length_sentence))
# model.add(Conv1D(90, filter_sizes[2]))
# model.add(MaxPooling1D(2))
# model.add(Dropout(0.5))
# model.add(Flatten())
# model.add(Dense(100, activation='relu'))
# #Use sigmoid activation for binary, softmax for multinomial
# model.add(Dense(number_labels, activation='sigmoid'))
# model.compile(loss='binary_crossentropy', optimizer='adam', metrics=[
#     'accuracy'])
# model.fit(train_comments_processed, experiment1, epochs=2,
#           batch_size=32, validation_split=0.1, shuffle=True)
# #we can also put cross-validation
# model.summary()

# maxlen = 100
# X_t = pad_sequences(list_tokenized_train, maxlen=maxlen)
# X_te = pad_sequences(list_tokenized_test, maxlen=maxlen)
#
# totalNumWords = [len(one_comment) for one_comment in list_tokenized_train]
#
# x = Embedding(voc_size, embedding_vector_length,
#                      input_length=max_length_sentence)
#
# x = LSTM(60, return_sequences=True,name='lstm_layer')(x)
#
# x = GlobalMaxPool1D()(x)
#
# x = Dropout(0.1)(x)
# x = Dense(50, activation="relu")(x)
# x = Dropout(0.1)(x)
# x = Dense(6, activation="sigmoid")(x)
# model = Model(inputs=inp, outputs=x)
# model.compile(loss='binary_crossentropy',
#                   optimizer='adam',
#                   metrics=['accuracy'])
# batch_size = 32
# epochs = 1
# model.fit(X_t,y, batch_size=batch_size, epochs=epochs, validation_split=0.1)

# results = model.evaluate(test_comments_processed, experimenttest)
# prediction = model.predict(test_comments_processed)
#
# submission_df = pd.DataFrame(columns=['id'] + labels)
# submission_df['id'] = test['id'].values
# submission_df[labels] = prediction
# submission_df.to_csv("./conv_result.csv", index=False)
# print(results)
# print(prediction)



